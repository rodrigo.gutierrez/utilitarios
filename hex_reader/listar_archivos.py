from argparse import ArgumentParser
import os
import datetime as dt
from pprint import pprint


def filtrar_carpetas(path:str , formato_fecha:str , formato_filtro:str) ->list:
    archivos = os.listdir(args.directorio)  

    datos_archivo = []
    for archivo in archivos:
        full_path = os.path.join(args.directorio,archivo) 
        is_file = True if os.path.isfile(full_path) else False
        status_archivo = os.stat(full_path)
        mtime = status_archivo.st_mtime
        fecha_archivo  = dt.datetime.fromtimestamp(mtime).strftime(formato_fecha) 
        objeto_fecha=dt.datetime.strptime(fecha_archivo,formato_fecha)
        
        #if fecha_archivo[:7] == dt.datetime.strptime(args.fecha[:7] , formato_filtro ):

        if args.fecha!=None:
            if fecha_archivo[:7] == args.fecha[:7]:
                if args.is_file =="true" and is_file==True:
                    datos_archivo.append( {"fecha" : fecha_archivo , "archivo" : full_path , "is_file" : is_file} )
                elif args.is_file =="false" and is_file==False: 
                    datos_archivo.append( {"fecha" : fecha_archivo , "archivo" : full_path , "is_file" : is_file} )
                elif args.is_file ==None: 
                    datos_archivo.append( {"fecha" : fecha_archivo , "archivo" : full_path , "is_file" : is_file} )
        else:
            datos_archivo.append( {"fecha" : fecha_archivo , "archivo" : full_path , "is_file" : is_file} )
    return datos_archivo


parser = ArgumentParser()
parser.add_argument("directorio" , type=str ,default="." , help = "ruta del directorio a listar")
parser.add_argument("--fecha" , type=str ,default=None , help=r"fecha a filtar formato 'Y-m' ")
parser.add_argument("--is_file",type = str ,default=None ,help="true o false sirve para ver si es un archivo")

args = parser.parse_args()
datos_archivo=filtrar_carpetas(path=args.directorio , formato_fecha="%Y-%m-%d %H:%M:%S" , formato_filtro="%Y-%m")



#pprint ( datos_archivo)
# if args.fecha != None : 
#     datos_archivo = list(filter(lambda archivo : archivo["fecha"] == args.fecha , datos_archivo)) 



pprint ( datos_archivo)