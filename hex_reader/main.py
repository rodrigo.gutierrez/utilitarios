import os
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", type=str, default="XD")
parser.add_argument("--numcol", type=int, default=4)

args = parser.parse_args()

if not os.path.isfile(args.file):
    raise Exception(f"El archivo [{args.file}] no existe")

archivo = open(args.file, "rb")
file_bytes = archivo.read()
cadena_base = "%0x\t"*args.numcol
tam = len(file_bytes)


# print(args.numcol)
for i in range(0, tam, args.numcol):

    # print ( list(file_bytes[i:i+args.numcol]) )
    data = list(file_bytes[i:i+args.numcol])

    # print ( data)
    if i+args.numcol < tam:
       print(cadena_base % (*file_bytes[i:i+args.numcol], ))
    if (tam-i) < args.numcol:
        tam_resto = args.numcol -  len(file_bytes[i:tam])
        
        
        fila_final = list(file_bytes[i:tam]) + list([0]*tam_resto)
        
        print ( cadena_base%(*fila_final,) ) 